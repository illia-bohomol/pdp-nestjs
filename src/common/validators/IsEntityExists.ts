import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  registerDecorator,
} from 'class-validator';
import { DataSource } from 'typeorm';

export function IsEntityExists() {
  return (object: object, propertyName: string): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      constraints: [],
      validator: IsEntityExistsConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'isEntityExists', async: true })
@Injectable()
export class IsEntityExistsConstraint implements ValidatorConstraintInterface {
  constructor(private readonly dataSource: DataSource) {}

  async validate(value: unknown, args: ValidationArguments): Promise<boolean> {
    const { property } = args;
    const entityName = this.getEntityName(property);

    const result = await this.dataSource
      .getRepository(entityName)
      .findOneBy({ id: value });

    return result ? true : false;
  }

  defaultMessage(args: ValidationArguments): string {
    const { value, property } = args;
    const entityName = this.getEntityName(property);

    return `${entityName} with id ${value} doesn't exist`;
  }

  getEntityName(property: string): string {
    return property[0].toUpperCase() + property.slice(1, -'Id'.length);
  }
}
