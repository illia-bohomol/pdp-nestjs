export const exhaustiveValueGuard = (value: any): never => {
  throw new Error(
    `ERROR! Reached forbidden guard function with unexpected value: ${JSON.stringify(
      value,
    )}`,
  );
};
