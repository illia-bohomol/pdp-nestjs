import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class PatchingMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    console.log('url', req.url);
    console.log('body', req.body);
    console.log('res', res.statusCode);
    next();
  }
}
