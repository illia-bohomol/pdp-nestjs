import { List } from '../../list/entities/list.entity';
import { Tag } from '../../tag/entities/tag.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 50 })
  title: string;

  @Column({
    default: false,
  })
  isCompleted: boolean;

  @ManyToOne(() => List, (list) => list.tasks, {
    nullable: false,
    cascade: true,
  })
  list: List;

  @ManyToMany(() => Tag, {
    cascade: true,
    nullable: true,
  })
  @JoinTable()
  tags: Tag[];
}
