import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { Tag } from '../tag/entities/tag.entity';
import { List } from '../list/entities/list.entity';
import { IsEntityExistsConstraint } from '../common/validators';

@Module({
  imports: [
    TypeOrmModule.forFeature([Task]),
    TypeOrmModule.forFeature([Tag]),
    TypeOrmModule.forFeature([List]),
  ],
  controllers: [TasksController],
  providers: [TasksService, IsEntityExistsConstraint],
  exports: [TasksService],
})
export class TasksModule {}
