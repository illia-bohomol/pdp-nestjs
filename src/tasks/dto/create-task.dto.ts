import {
  IsArray,
  IsInt,
  IsOptional,
  IsString,
  Length,
  Min,
} from 'class-validator';
import { IsEntityExists } from '../../common/validators';

export class CreateTaskDto {
  @IsEntityExists()
  @IsInt()
  @Min(1)
  listId: number;

  @IsString()
  @Length(1, 50)
  title: string;

  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  tagIds: number[];
}
