import { PartialType } from '@nestjs/mapped-types';
import { IsBoolean, IsOptional } from 'class-validator';
import { IsEntityExists } from '../../common/validators/IsEntityExists';
import { CreateTaskDto } from './create-task.dto';

export class UpdateTaskDto extends PartialType(CreateTaskDto) {
  @IsEntityExists()
  listId: number;

  @IsOptional()
  @IsBoolean()
  isCompleted: boolean;
}
