import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { List } from '../list/entities/list.entity';
import { Tag } from '../tag/entities/tag.entity';
import { In, Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task) private taskRepository: Repository<Task>,
    @InjectRepository(Tag) private tagRepository: Repository<Tag>,
    @InjectRepository(List) private listRepository: Repository<List>,
  ) {}

  async create({ tagIds, listId, title }: CreateTaskDto) {
    const newTask = new Task();
    newTask.title = title;

    if (tagIds?.length) {
      const tags = await this.tagRepository.findBy({
        id: In(tagIds),
      });

      newTask.tags = tags;
    }

    const list = await this.listRepository.findOneBy({ id: listId });

    if (!list) {
      throw new Error('List not found');
    }

    newTask.list = list;

    return this.taskRepository.save(newTask);
  }

  findAll() {
    return this.taskRepository.find({ loadRelationIds: true });
  }

  findOne(id: number) {
    return this.taskRepository.findOne({
      where: { id },
      relations: {
        tags: true,
        list: true,
      },
    });
  }

  async update(id: number, { tagIds, listId, title }: UpdateTaskDto) {
    const newTask = await this.taskRepository.preload({ id, title });

    if (!newTask) {
      throw new Error('Task not found');
    }

    if (tagIds?.length) {
      const tags = await this.tagRepository.findBy({
        id: In(tagIds),
      });

      newTask.tags = tags;
    }

    if (listId) {
      const list = await this.listRepository.findOneBy({ id: listId });

      if (!list) {
        throw new Error('List not found');
      }

      newTask.list = list;
    }

    return this.taskRepository.save(newTask);
  }

  remove(id: number) {
    return this.taskRepository.delete(id);
  }
}
