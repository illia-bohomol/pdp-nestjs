import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { exhaustiveValueGuard } from 'src/common/utils/exhaustiveValueGuard';

const baseConfig: TypeOrmModuleOptions = {
  type: 'mariadb',
  host: 'localhost',
  port: 3306,
  username: 'root',
  autoLoadEntities: true,
  synchronize: true,
};

export const getDbConfig = (): TypeOrmModuleOptions => {
  let config = baseConfig;
  const env = process.env.NODE_ENV;

  switch (env) {
    case 'development': {
      config = {
        ...baseConfig,
        database: 'pdp',
      };

      break;
    }

    case 'test': {
      config = {
        ...baseConfig,
        database: 'pdp_test',
      };

      break;
    }

    default: {
      exhaustiveValueGuard(env);
    }
  }

  return config;
};
