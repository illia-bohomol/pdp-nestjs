import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ListService } from './list.service';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { IsEntityExists } from '../common/validators';

class UpdateParams {
  @IsEntityExists()
  listId: number;
}

@Controller('lists')
export class ListController {
  constructor(private readonly listsService: ListService) {}

  @Post()
  create(@Body() createListsDto: CreateListDto) {
    return this.listsService.create(createListsDto);
  }

  @Get()
  findAll() {
    return this.listsService.findAll();
  }

  @Get(':listId')
  findOne(@Param() { listId }: UpdateParams) {
    return this.listsService.findOne(+listId);
  }

  @Patch(':listId')
  update(
    @Param('listId') listId: string,
    @Body() updateListsDto: UpdateListDto,
  ) {
    return this.listsService.update(+listId, updateListsDto);
  }

  @Delete(':listId')
  remove(@Param('listId') listId: string) {
    return this.listsService.remove(+listId);
  }
}
