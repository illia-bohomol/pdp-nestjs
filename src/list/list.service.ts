import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { List } from './entities/list.entity';
import { CreateListDto, UpdateListDto } from './dto';

@Injectable()
export class ListService {
  constructor(
    @InjectRepository(List) private listRepository: Repository<List>,
  ) {}

  create(createListDto: CreateListDto) {
    return this.listRepository.save(createListDto);
  }

  findAll() {
    return this.listRepository.find();
  }

  findOne(id: number) {
    return this.listRepository.findOne({ where: { id } });
  }

  update(id: number, updateListDto: UpdateListDto) {
    return this.listRepository.update(id, updateListDto);
  }

  remove(id: number) {
    return this.listRepository.delete(id);
  }
}
