import { Body, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { Tag } from './entities/tag.entity';

@Injectable()
export class TagService {
  constructor(
    @InjectRepository(Tag) private readonly tagRepository: Repository<Tag>,
  ) {}

  create(createTagDto: CreateTagDto) {
    return this.tagRepository.save(createTagDto);
  }

  findAll() {
    return this.tagRepository.find();
  }

  findOne(id: number) {
    return this.tagRepository.findBy({ id });
  }

  async update(id: number, @Body() updateTagDto: UpdateTagDto) {
    const updatedTag = await this.tagRepository.preload({
      id,
      ...updateTagDto,
    });

    if (!updatedTag) {
      throw new Error('Tag not found');
    }

    return this.tagRepository.save(updatedTag);
  }

  remove(id: number) {
    return this.tagRepository.delete({ id });
  }
}
