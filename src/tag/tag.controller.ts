import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TagService } from './tag.service';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { IsEntityExists } from '../common/validators';

class UpdateParams {
  @IsEntityExists()
  tagId: number;
}

@Controller('tags')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @Post()
  create(@Body() createTagDto: CreateTagDto) {
    return this.tagService.create(createTagDto);
  }

  @Get()
  findAll() {
    return this.tagService.findAll();
  }

  @Get(':tagId')
  findOne(@Param('tagId') tagId: string) {
    return this.tagService.findOne(+tagId);
  }

  @Patch(':tagId')
  update(@Param() { tagId }: UpdateParams, @Body() updateTagDto: UpdateTagDto) {
    return this.tagService.update(+tagId, updateTagDto);
  }

  @Delete(':tagId')
  remove(@Param('tagId') tagId: string) {
    return this.tagService.remove(+tagId);
  }
}
