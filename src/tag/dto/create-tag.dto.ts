import { IsString, Length } from 'class-validator';

export class CreateTagDto {
  @IsString()
  @Length(1, 50)
  name: string;

  @IsString()
  @Length(7, 7)
  color: string;
}
