import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { TasksService } from '../src/tasks/tasks.service';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../src/app.module';

describe('Tasks', () => {
  let app: INestApplication;
  let tasksService: TasksService;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    tasksService = moduleRef.get<TasksService>(TasksService);
    await app.init();
  });

  it(`/GET tasks`, () => {
    return request(app.getHttpServer())
      .get('/tasks')
      .expect(200)
      .expect(tasksService.findAll());
  });

  it(`/GET task`, () => {
    return request(app.getHttpServer())
      .get('/tasks/1')
      .expect(200)
      .expect(tasksService.findOne);
    // .expect(tasksService.g());
  });

  afterAll(async () => {
    await app.close();
  });
});
